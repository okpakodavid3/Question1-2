package test2;

import java.util.Arrays;
import java.util.Scanner;

public class Test2 {

    static Scanner userIn = new Scanner(System.in);

    public static void main(String[] args) {
        // Question 1
        String num1 = userIn.next();
        String num2 = userIn.next();
        String operator = userIn.next();

        System.out.println(calculate(num1, num2, operator));

        // Question 2
        int[] tom = {1, 4, 7, 2, 4}; // 0 0 1 0 0
        int[] jack = {3, 4, 2, 4, 4}; // 1 0 0 1

        System.out.println(Arrays.toString(scoreBoard(tom, jack)));
    }

    public static int[] scoreBoard(int[] a1, int[] a2) {
        int[] board = new int[2];
        for (int i = 0; i < a1.length; i++) {
            int t = a1[i];
            int j = a2[i];

            if (j == t) {
                a2[i] = 0;
                a1[i] = 0;
            } else if (j < t) {
                a2[i] = 0;
                a1[i] = 1;
            } else {
                a2[i] = 1;
                a1[i] = 0;
            }

            if (i == a1.length - 1) {
                board[0] = sum(a1);
                board[1] = sum(a2);
            }
        }

        return board;
    }

    public static int sum(int[] arr) {
        int count = 0;
        int sum = 0;
        while ((count++) < arr.length) sum += arr[count - 1];
        
        return sum;
    }

    public static String calculate(String num1, String num2, String operand) {
        int n1 = parse(num1);
        int n2 = parse(num2);
        int n3 = 0;
        boolean error = false;

        switch (operand) {
            case "/":
                n3 = n1 / n2;
                break;
            case "*":
                n3 = n1 * n2;
                break;
            case "+":
                n3 = n1 + n2;
                break;
            case "-":
                n3 = n1 - n2;
                break;
            default:
                error = true;
                break;
        }

        if (error) {
            throw new Error("Bad operand passed");
        }

        return n3 + "";
    }

    public static int parse(String text) {
        int size = text.length();
        int result = 0;

        boolean error = false;

        for (int i = 0; i < size; i++) {
            //get the char value and subtract from 48;
            int charValue = text.charAt(i);
            int num = charValue - 48;

            if (num > 9) {
                error = true;
            }

            result = result * 10 + (num);
        }

        if (error) {
            throw new NumberFormatException("Sorry one of the inputs is not a number");
        }

        return result;
    }

}
